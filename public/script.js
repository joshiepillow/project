"use strict";
/*
class change {
  constructor(name, info, 
    action = () => {this.info += 1;}
  ) {
    this.name = name;
    this.info = info;
    this.action = action;
    this.time = 100;
    this.counter = 0;
  }
}
var hi = new change("1", 0);
var ho = new change("2", 0);
function run(h) {
  if (!counters.includes(window[h])) {
    counters.push(window[h]);
    window[h].counter = 0;
  }

}
let counters = [];
requestAnimationFrame(facilitator)
function facilitator() {
  //if (!counters) {
  //  let counters = {};
  }//
  for (item in counters) {
    if (counters[item].time > counters[item].counter) {
      counters[item].action();
      counters[item].counter++;
      disp()
    } else {
      counters.splice(item, 1)
    }
  }
  requestAnimationFrame(facilitator)
}
*/

//A obstacle...blocks chara
//Give x, y, height, and width of object, must be rectangle
//function collide: takes x, y, height, and width of other object... sees if they collide
class Obstacle {
  constructor(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }
  
  collide(itemX, itemY, itemWidth, itemHeight) {
    if (((itemX + itemWidth > this.x) && (itemX < (this.x + this.width))) && ((itemY + itemHeight > this.y) && (itemY < (this.y + this.height)))) {
      return false;
    } else {
      return true;
    }
  }
}

//takes x, y, and dist... returns array of direction
function findDirect(x, y, dist){
  let a = Math.pow(x, 2) + Math.pow(y, 2);
  let b = Math.sqrt(Math.pow(dist, 2)/a);
  return [b*x, b*y]
}


function time() {
  if (counter < 200) {
  animateAtk(counter);
  timeID = requestAnimationFrame(time)
  counter++;
  } else {
    endAtk(timeID);
  }
}

function animateAtk(count) {
  attacks["x"] += attacks["direction"][0];
  attacks["y"] += attacks["direction"][1];
  redraw();
  console.log("tried")
}

function endAtk(ID) {
  cancelAnimationFrame(ID);
  counter = 0;
}

function addListeners(list){
  for (let key in list) {
    document.getElementById(key).addEventListener("click", function(event) {
      listen(list[key]);
      event.stopPropagation();
    });
  }
}

function attack(event) {
  endAtk(timeID);
  attacks["x"] = mainchara["center"]()["x"];
  attacks["y"] = mainchara["center"]()["y"];
  attacks["direction"] = findDirect(event.clientX - mainchara["center"]()["x"], event.clientY - mainchara["center"]()["y"], 1);
  requestAnimationFrame(time);
}


function listen(key) {
  let fakeX = mainchara["x"];
  let fakeY = mainchara["y"];
  if (key == 37 || key == 65) {
    fakeX -= 10;
  } else if (key == 38 || key == 87) {
    fakeY -= 10;
  } else if (key == 39 || key == 68) {
    fakeX += 10;
  } else if (key == 40 || key == 83) {
    fakeY += 10;
  } else {

  }
  if (((fakeX >= 0) && (fakeX <= (window.innerWidth - 50))) && ((fakeY >= 0) && (fakeY <= (window.innerHeight - 50)))) {
    if (obstacles.every(x => x.collide(fakeX, fakeY, 50, 50))) {
      
      mainchara["x"] = fakeX;
      mainchara["y"] = fakeY;
      redraw();
    }  
  }
}

function redraw () {
  let c = document.getElementById("can");
  let can = c.getContext("2d");
  can.canvas.width = window.innerWidth;
  can.canvas.height = window.innerHeight;
  can.beginPath();
  can.rect(mainchara["x"], mainchara["y"], 50, 50);
  can.stroke();
  for (let item in obstacles) {
    can.beginPath();
    can.rect(obstacles[item].x, obstacles[item].y, obstacles[item].width, obstacles[item].height)
    can.stroke();
  }
  can.beginPath();
  can.arc(attacks["x"], attacks["y"], 5, 0, 2*Math.PI);
  can.stroke();
};

let mainchara = {
  x: 50,
  y: 50,
  width: 50,
  height: 50,
  center: () => {
    return {x: mainchara["x"] + mainchara["width"]/2, y: mainchara["y"] + mainchara["height"]/2}
  }
}



let htmlClicks = {
  "up": 38,
  "left": 37,
  "down": 40,
  "right": 39
};
let timeID = 1;
let obstacles = [
  new Obstacle(200, 200, 200, 200)
];
let attacks = {x: -100, y: -100, direction: 0}
let counter;
addListeners(htmlClicks);
document.addEventListener("click", attack);
document.addEventListener("keydown", function(event) {
  console.log(event.which);
});
document.onkeydown = function(e) {
    e = e || window.event;
/*    switch(e.which || e.keyCode) {
      case 37: // left  
      case 38: // up
      case 39: // right
      case 40: // down
        listen(e.which || e.keyCode);
        break;
      default: return; // exit this handler for other keys
    }*/
    listen(e.which || e.keyCode);
    e.preventDefault(); // prevent the default action (scroll / move caret)
};


redraw();